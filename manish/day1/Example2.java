package manish.day1;

public class Example2 {
public static void main(String[] args) {
	String name = "manpreet";
	String company = "RBC";
	int currentYear = 2022;
	String currentMonth; // Just declaring the variable and value will be assigned later 
	final int daysInCurrentMonth = 30;
	System.out.println(name);
	System.out.println(company);
	System.out.println(currentYear);
	currentMonth = "April"; // Assigning the value to the variable
	System.out.println(currentMonth);
	currentYear = 2024;
	System.out.println(currentYear);

	System.out.println(daysInCurrentMonth);

	
}
}
