package manish.day2;
// Example for demonstrating some methods of String class
public class Example6 {
public static void main(String[] args) {
	String name = "Manpreet";
	String txt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	System.out.println(name.length());
	System.out.println(txt.length());
	System.out.println("The length of the txt string is " + txt.length());
	
	// Now trying some more methods of String
	System.out.println(name.toUpperCase());
	System.out.println(name.toLowerCase());
	System.out.println(txt.toLowerCase());
	
}
}
