package manpreet.day2;
// This is an example of Logical And
public class Example3 {
	public static void main(String[] args) {
		int x = 5;
		System.out.println(x > 3 && x < 10); // returns true because 5 is greater than 3 AND 5 is less than 10
	}

}
