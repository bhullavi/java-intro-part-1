package manish.day2;

// Concatenating number and a String in differnt orders
public class Example9 {

	public static void main(String[] args) {
		
// Adding two int values
		int x1 = 10;
		int y1 = 20;
		System.out.println(x1 + y1);

// Adding String to an int 
		String x2 = "10";
		int y2 = 20;
		System.out.println(x2 + y2);

// Adding two Strings
		String x3 = "10";
		String y3 = "20";
		System.out.println(x3 + y3);

// Adding int to a String
		int x4 = 10;
		String y4 = "20";
		System.out.println(x4 + y4);
	}

}
