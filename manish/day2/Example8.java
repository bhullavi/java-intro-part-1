package manish.day2;

public class Example8 {
// Concatenation the String values
	public static void main(String[] args) {
		String firstName = "John ";
		String lastName = "Doe";
		System.out.println(firstName +  lastName);
		System.out.println(firstName.concat(lastName));
	}

}
