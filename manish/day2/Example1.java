package manish.day2;
// Widening Cast
public class Example1 {
	public static void main(String[] args) {
		int myInt = 23;
		long myLong = myInt;
		double myDouble = myInt;
		System.out.println(myInt);
		System.out.println(myLong);
		System.out.println(myDouble);
		
	}

}
