package manpreet.day2;

// Handling Special Characters
public class Example10 {

	public static void main(String[] args) {
		String txt = "We are the so-called \"Vikings\" from the north.";
		System.out.println(txt);

		String txt2 = "It's alright.";
		System.out.println(txt2);

		String txt3 = "The character \\ is called backslash.";
		System.out.println(txt3);
		
		String txt4 = "John\tDoe";
		System.out.println(txt4);



	}

}
