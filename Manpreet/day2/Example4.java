package manpreet.day2;
// Example of Logical OR
public class Example4 {
	public static void main(String[] args) {
		int x = 2;
		System.out.println(x > 3 || x < 4); // returns true because one of the conditions are true (5 is greater than 3,
											// but 5 is not less than 4)
	}

}
