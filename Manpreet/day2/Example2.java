package manpreet.day2;
// Narrowing Cast
public class Example2 {
	public static void main(String[] args) {
		double myDouble = 20.4;
		int myInt = (int) myDouble;
		System.out.println(myDouble);
		System.out.println(myInt);
		
	}

}
