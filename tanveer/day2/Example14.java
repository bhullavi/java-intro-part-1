package tanveer.day2;

public class Example14 {
	// Example of logical "AND" operator.
		  public static void main(String[] args) {
		    int x = 5;
		    System.out.println(x > 90 && x < 10); // returns true because 5 is greater than 3 AND 5 is less than 10
		  }

}
