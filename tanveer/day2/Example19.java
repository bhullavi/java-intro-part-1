package tanveer.day2;

public class Example19 {
	// Example of location of a word in a given sentence.
	public static void main(String[] args) {
	    String txt = "Please locate where 'Tanveer' occurs!";
	    System.out.println(txt.indexOf("Tanveer"));
	  }

}
