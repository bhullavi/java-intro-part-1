package tanveer.day2;

public class Example3 {
	// Demo for boolean variable
public static void main(String[] args) {
	
	boolean isJavaFun = true;
	boolean isFishTasty = false;
	System.out.println(isJavaFun);     // Outputs true
	System.out.println(isFishTasty);   // Outputs false
}
}
