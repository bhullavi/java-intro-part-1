package tanveer.day2;

public class Example4 {
	//Demo of wideing of cast
	public static void main(String[] args) {
		
			    int myInt = 9;
			    double myDouble = myInt; // Automatic casting: int to double

			    System.out.println(myInt);      // Outputs 9
			    System.out.println(myDouble);   // Outputs 9.0
			  
	}

}
