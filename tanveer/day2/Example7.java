package tanveer.day2;

public class Example7 {
	//Demo of different operators
public static void main(String[] args) {
	int x = 10;
	int y = 2;
	System.out.println(x+y);
	System.out.println(x-y);
	System.out.println(x*y);
	System.out.println(x/y);
	System.out.println(x%y);
	//demo of ++ and -- operators
	System.out.println(x);
	System.out.println(x--);
	System.out.println(x);
	System.out.println(y);
	System.out.println(y++);
	System.out.println(y);
	int z= 12;
	 z++;
	System.out.println(z);
	/* There are two types of increment or decrement operators-
	  1. Pre-increment or Pre- decrement - In this case, we get the updated value in the sysout where this operation is being operated.
	  2. Post- increment or Post-decrement - In this case, we get the old value in the sysout where this operation is being operated. And the updated value becomes available only after we move to the next statement.
	  
	 
	 */
}
}
