package tanveer.day2;

public class Example5 {
	//Demo of narrowing of cast
	public static void main(String[] args) {
		
			    double myDouble = 9.12;
			    int myInt = (int) myDouble; // No Automatic casting: double to int

			    System.out.println(myInt);      // Outputs 9
			    System.out.println(myDouble);   // Outputs 9.12
			  
	}

}
