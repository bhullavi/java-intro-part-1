package tanveer.day2;

public class Example12 {
	// Example of greater then = operator.
	public static void main(String[] args) {
	    int x = 3;
	    int y = 3;
	    System.out.println(x >= y); // returns true because 5 is greater, or equal, to 3
	  }

}
