package tanveer.day2;

public class Example18 {
	// Example of changing to upper and lower case.
	 public static void main(String[] args) {
		    String txt = "JAVA";
		    System.out.println(txt.toUpperCase());
		    System.out.println(txt.toLowerCase());
		  }

}
