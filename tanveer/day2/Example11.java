package tanveer.day2;

public class Example11 {
	// Example of less then operator.
	public static void main(String[] args) {
	    int x = 5;
	    int y = 3;
	    System.out.println(y < x); // returns false because 5 is not less than 3
	  }

}
