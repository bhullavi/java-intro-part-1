package tanveer.day2;

public class Example9 {
	// Example to not equal operator
	 public static void main(String[] args) {
		    int x = 5;
		    int y = 3;
		    System.out.println(x != y); // returns true because 5 is not equal to 3
		  }

}
