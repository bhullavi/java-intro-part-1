package tanveer.day2;

public class Example13 {
	// Example of less then = operator.
	public static void main(String[] args) {
	    int x = 5;
	    int y = 3;
	    System.out.println(x <= y); // returns false because 5 is neither less than or equal to 3
	  }

}
