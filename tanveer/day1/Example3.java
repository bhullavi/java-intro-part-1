package tanveer.day1;

public class Example3 {
	public static void main(String[] args) {
		String firstname = "Tanveer";
		String lastname = "Singh";
		System.out.println(firstname);
		System.out.println(lastname);
		System.out.println(firstname+" "+lastname);
		int myNum = 15;
		System.out.println(myNum);
		myNum = 20;  // myNum is now 20
		System.out.println(myNum);
		
		final int myNum2 = 23;
		myNum2 = 20;  // will generate an error: cannot assign a value to a final variable
	}

}
